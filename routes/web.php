<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('layouts.app');
})->middleware(['auth.shopify', 'billable'])->name('home');

Route::group(["middleware" => ["auth.shopify"],"prefix" => "choose-plan"], function(){
    Route::get("/free", "ChoosePlan\ChoosePlanController@chooseFreePlan")->name('choose.plan.free');
    Route::get("/{is_disable_free}", "ChoosePlan\ChoosePlanController@index")->name('choose.plan.index');
});

Route::group(['middleware' => ['auth.shopify', "billable"] ], function () {
    Route::get('search','Ruleset\RulesetController@search');
    Route::get('discounted','Ruleset\RulesetController@discountedAPIIndex');
    Route::get('ruleset/delete-product/{id}','Ruleset\RulesetController@deleteProduct');
    Route::get('ruleset/setting','Ruleset\RulesetController@getSetting');
    Route::post('ruleset/check','Ruleset\RulesetController@check');
    Route::post('ruleset/setting','Ruleset\RulesetController@storeSetting');
    Route::post('ruleset/app-status','Ruleset\RulesetController@appStatus');
    Route::get('ruleset/plan-details','Ruleset\RulesetController@getPlanDetails');
    Route::post('ruleset/{ruleset}','Ruleset\RulesetController@update')->name('ruleset.update');
    Route::resource('ruleset','Ruleset\RulesetController');
    Route::get('test','Test\TestController@index');
    Route::get('installation-guide', function (){
        return view('pages.installation_guide') ;
    });
});


Route::get('flush', function(){
    request()->session()->flush();
});
