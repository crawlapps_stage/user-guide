<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace' => 'API'], function () {

    Route::get('version', function(Request $request) {

        $jsFilePath = public_path() . '/js/crawlapps-userguide.js';
        $jsVersion  = filemtime($jsFilePath);

        return response()->json(['data' => ['js' => $jsVersion]]);
    });

    Route::get('/pdf/{user_id}/{product_id}', 'CodeController@pdf');

    Route::group(['prefix' => '{shopifyShop}'], function () {
        Route::get('code', 'CodeController@index');
        Route::get('page', 'PageController@index');
        Route::get('page-pdfdownload', 'PageController@downloadPDF');
    });

    Route::post('store-images', 'CodeController@storeImages')->name('storeimages'); // to add images of metafield value
});
Route::post('store-pdf', 'Ruleset\RulesetController@storePDF')->name('storepdf'); // to add images of metafield value
