<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Choose Plan</title>
    <link href="https://fonts.googleapis.com/css2?family=Quicksand:wght@300;400;500;600;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://sdks.shopifycdn.com/polaris/3.11.0/polaris.min.css" />
    <link rel="stylesheet" href="{{asset('css/app.css')}}" type="text/css" />

    @if(config('shopify-app.appbridge_enabled'))
        <script src="https://unpkg.com/@shopify/app-bridge"></script>
        <script>
            var AppBridge = window['app-bridge'];
            var createApp = AppBridge.default;

            window.shopify_app_bridge = createApp({
                apiKey: '{{ config('shopify-app.api_key') }}',
                shopOrigin: '{{ Auth::user()->name }}',
                forceRedirect: true,
            });
        </script>
    @endif

</head>
<body class="">
@if( \Auth::user()->plan_id != '' && $is_disable_free == 0 )
<div id="app">
</div>
@endif
<div class="wrapper" id="plan-price-header" style="display: block;">
    <h1 class="text-center">CHOOSE SUBSCRIPTION PLAN</h1>
    @if( $is_disable_free == 1 )
        <h3 class="text-center" style="color:red;"> You need to choose any plan(Basic/Pro). </h3>
    @endif
</div>

<div class="choose-plan-row" id="plan-price"  style="display: flex;">
    <div class="col">
        <div class="card">
            <div class="card-header">
                <h2 class="title">Basic</h2>
                <p class="price">
                    5$
                </p>
                <span class="description">Perfect for Online Store</span>
                <div class="line"></div>
            </div>

            <ul class="features">
                <li><i class="fa fa-check"></i> Create up to 15 User guides</li>
                <li><i class="fa fa-check"></i> Upload existing user guides instead of having to create them if you already have some
                <li><i class="fa fa-check"></i> Page that regroups every user guides</li>
                <li><i class="fa fa-check"></i> Possibility to add a link or a button for a user guide directly on the product page that the customer is viewing.</li>
                <li><i class="fa fa-check"></i> Advanced Customization available</li>
                <li><i class="fa fa-check"></i> Increases your customer satisfaction</li>
            </ul>
            @if(\Auth::user()->plan_id == 1)
                <div class="alert alert-success">
                    Current Plan
                </div>
            @else
                <a href="{{ route('billing', ['plan' => 1])}}" target="_parent" class="btn btn-success">Select</a>
            @endif
        </div>
    </div>
    <div class="col">
        <div class="card">
            <div class="card-header">
                <h2 class="title">Pro</h2>
                <p class="price">
                    10$
                </p>
                <span class="description">Perfect for Online Store</span>
                <div class="line"></div>
            </div>

            <ul class="features">
                <li><i class="fa fa-check"></i> Create unlimited number of user guides</li>
                <li><i class="fa fa-check"></i> Upload existing user guides instead of having to create them if you already have some
                <li><i class="fa fa-check"></i> Page that regroups every user guides</li>
                <li><i class="fa fa-check"></i> Possibility to add a link or a button for a user guide directly on the product page that the customer is viewing.</li>
                <li><i class="fa fa-check"></i> Advanced Customization available</li>
                <li><i class="fa fa-check"></i> Increases your customer satisfaction</li>
            </ul>
            @if(\Auth::user()->plan_id == 2)
                <div class="alert alert-success">
                    Current Plan
                </div>
            @else
                <a href="{{ route('billing', ['plan' => 2])}}" target="_parent" class="btn btn-success">Select</a>
            @endif
        </div>
    </div>
</div>
</body>
<script> window.planID = {{\Auth::user()->plan_id}};</script>
<script src="{{  asset(mix('js/app.js'))  }}"></script>
<script>
    window.addEventListener("load", function() {
        document.getElementById("plan-pricing").classList.add("Polaris-Tabs__Tab--selected");
    });
</script>
</html>
