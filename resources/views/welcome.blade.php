<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="csrf-url" content="{{ env('APP_API_URL') }}">
    <!-- Fonts -->
    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<style type="text/css">
    @media print {
        .element-that-contains-table {
            overflow: visible !important;
        }
    }

    thead{display: table-header-group;}
    tfoot {display: table-row-group;}
    tr {page-break-inside: avoid;}
    table {
        text-align: center;
        border-collapse: collapse;
        border-spacing: 0;
        margin: auto;
        border: 1px double #b3b3b3;
        width: 100%;
    }
    table td, table table th {
        min-width: 2em;
        padding: .6rem;
        border: 1px solid #bfbfbf;
    }
    /*table, th, td {*/
    /*    border: 2px solid black;*/
    /*}*/
    table td {
        border: 2px solid black;
        word-wrap: break-word;
        /*overflow: initial;*/
    }
    .image img{
        display: block !important;
        max-width: 100%;
        height: auto;
        margin-left: auto !important;
        margin-right: auto !important;
    }

    td .image img{
        max-width: 100%;
        float: left !important;
        clear: both;
        width: auto;
        height: auto;
    }

    .image-style-align-left img{
        max-width: 100%;
        float: left !important;
        clear: both;
        width: auto;
        height: auto;
    }

    .image-style-align-right img{
        max-width: 100%;
        float: right !important;
        clear: both;
        width: auto;
        height: auto;
    }

    figcaption{
        text-align: center;
    }
</style>
</head>
<body>
{!! $html !!}
</body>
</html>
