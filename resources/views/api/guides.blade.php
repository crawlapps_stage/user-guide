<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
@php
     $heading1 = $style['heading1'];
     $heading2 = $style['heading2'];
     $button = $style['button'];
     $link = $style['link'];
     $product_css = $style['product'];
     $search = $style['search']
@endphp

<div class="user-guide">
    @if( $search['status'] === 1 )
        <div class="search-container">
            <form class="guide-search">
                <input type="text" placeholder="Search.." id="guide-search-id" name="search" value="{{$search_value}}">
                <button type="button" id="btn-search-guide"><i class="fa fa-search"></i></button>
            </form>
        </div>
    @endif

    @if( count($products) > 0 )
        <p class="guide-heading1">{{$heading1['label']}}</p>
        <p class="guide-heading2">{{$heading2['label']}}</p>

            @foreach( $products as $key=>$val )
                <button  class="accordion guide-product-title">{{$val->product_title}}</button>
                <div class="panel">
                    @if ( $val->belongs_to_ruleset->guide_type === 0 )
                        @php $downloadlink = config('app.url')."/api/pdf/".$val->user_id."/".$val->product_id; @endphp
                        @if( $link['status'] == 1 )
                            <a class="guide-link" href="#" id="crawlapps_userguide_pdf" data-value="{{$downloadlink}}" data-guide-type="0" data-guide-id="{{$val->belongs_to_ruleset->id}}" > {{$link['label']}}</a>
                        @endif
                        @if( $button['status'] == 1 )
                            <a href="#" type="button" class="guide-button" id="crawlapps_userguide_pdf" data-value="{{$downloadlink}}" data-guide-type="0" data-guide-id="{{$val->belongs_to_ruleset->id}}">{{$button['label']}}</a><br>
                        @endif
                    @elseif( $val->belongs_to_ruleset->guide_type === 1 )
                        @php $downloadlink = 'href=' . \Storage::disk('public')->url('uploads/'.$val->belongs_to_ruleset->description) . ' download target=_blank '; @endphp
                        @if( $link['status'] == 1 )
                            <a class="guide-link" id="crawlapps_userguide_pdf" {{$downloadlink}} data-guide-type="1" data-guide-id="{{$val->belongs_to_ruleset->id}}"> {{$link['label']}}</a>
                        @endif
                        @if( $button['status'] == 1 )
                            <a type="button" class="guide-button" id="crawlapps_userguide_pdf" {{$downloadlink}} data-guide-type="1" data-guide-id="{{$val->belongs_to_ruleset->id}}">{{$button['label']}}</a><br>
                        @endif
                    @endif
                </div>
            @endforeach
        </div>
    @else
        <h3 class="not-found"><center>Product not found...</center></h3>
    @endif

<style>
    .user-guide{
        margin: 0 20%;
    }
    .guide-heading1{
        text-decoration: {{$heading1['text_decoration_line']}} {{$heading1['text_decoration_style']}} {{$heading1['text_decoration_color']}} !important;
        color: {{$heading1['text_color']}} !important;
        font-size: {{$heading1['text_size']}}px !important;
        font-weight: {{$heading1['text_weight']}} !important;
    }
    .guide-heading2{
        text-decoration: {{$heading2['text_decoration_line']}} {{$heading2['text_decoration_style']}} {{$heading2['text_decoration_color']}} !important;
        color: {{$heading2['text_color']}} !important;
        font-size: {{$heading2['text_size']}}px !important;
        font-weight: {{$heading2['text_weight']}} !important;
    }
    .guide-product-title{
        text-decoration: {{$product_css['text_decoration_line']}} {{$product_css['text_decoration_style']}} {{$product_css['text_decoration_color']}} !important;
        color: {{$product_css['text_color']}} !important;
        font-size: {{$product_css['text_size']}}px !important;
        font-weight: {{$product_css['text_weight']}} !important;
        {{--font-family: {{$product_css['font_family']}} !important;--}}
    }
    .guide-button{
        border: 1px solid;
        width: fit-content;
        margin: 20px 20px 0px 20px !important;
        color: {{$button['text_color']}} !important;
        background-color: {{$button['bg_color']}} !important;
        font-size: {{$button['text_size']}}px !important;
        padding: {{$button['padding_top']}}px {{$button['padding_right']}}px {{$button['padding_bottom']}}px {{$button['padding_left']}}px !important;
        border-width: {{$button['border_size']}}px !important;
        border-radius: {{$button['border_radius']}}px !important;
        border-color: {{$button['border_color']}} !important;
    }
    .guide-button:hover{
        border-color: {{$button['border_color_hover']}} !important;
    }
    .guide-link{
        padding: 20px  !important;
        text-decoration: {{$link['text_decoration_line']}} {{$link['text_decoration_style']}} {{$link['text_decoration_color']}} !important;
        color: {{$link['text_color']}} !important;
        font-size: {{$link['text_size']}}px !important;
        font-weight: {{$link['text_weight']}} !important;
        {{--font-family: {{$link['font_family']}} !important;--}}
    }
    .user-guide .accordion {
        background-color: {{$product_css['bg_color']}};
        color: #444;
        cursor: pointer;
        padding: 18px;
        width: 100%;
        border: none;
        text-align: left;
        outline: none;
        font-size: 15px;
        transition: 0.4s;
    }
    .not-found{
        margin-top: 20px;
    }
    .user-guide .active, .accordion:hover {
        background-color: {{$product_css['bg_color_on_hover']}};
    }

    .user-guide .accordion:after {
        content: '\002B';
        color: #777;
        font-weight: bold;
        float: right;
        margin-left: 5px;
    }

    .user-guide .active:after {
        content: "\2212";
    }

    /*.panel {*/
    /*    padding: 0 18px;*/
    /*    background-color: white;*/
    /*    max-height: 0;*/
    /*    overflow: hidden;*/
    /*    transition: max-height 0.2s ease-out;*/
    /*}*/
    .user-guide .panel{
        overflow: hidden;
        position: relative;
        display: -ms-flexbox;
        display: flex;
        max-height: 0;
        -ms-flex-direction: column;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 1px solid rgba(0,0,0,.125);
        border-radius: .25rem;
        transition: max-height 0.2s ease-out;
    }
    /* Style the search field */
    form.guide-search input[type=text] {
        color: {{$search['text_color']}};
        padding: {{$search['padding_top']}}px {{$search['padding_right']}}px {{$search['padding_bottom']}}px {{$search['padding_left']}}px;
        font-size: {{$search['text_size']}}px;
        border: {{$search['border_size']}}px solid {{$search['border_color']}};
        float: left;
        width: 80%;
        background: {{$search['bg_color']}};
        border-radius: {{$search['border_radius']}}px 0px 0px {{$search['border_radius']}}px;
    }

    /* Style the submit button */
    form.guide-search button {
        color: {{$search['sb_text_color']}};
        padding: {{$search['sb_padding_top']}}px {{$search['sb_padding_right']}}px {{$search['sb_padding_bottom']}}px {{$search['sb_padding_left']}}px;
        font-size: {{$search['sb_text_size']}}px;
        border: {{$search['sb_border_size']}}px solid {{$search['sb_border_color']}};
        background: {{$search['sb_bg_color']}};
        border-radius: 0px {{$search['border_radius']}}px {{$search['border_radius']}}px 0px;
        float: left;
        width: 20%;
        border-left: none; /* Prevent double borders */
        cursor: pointer;
    }

    form.guide-search{
        margin:auto;
        max-width:300px;
        /*float: right;*/
    }

    form.guide-search button:hover {
        background: #8a8a8a;
    }

    /* Clear floats */
    form.guide-search::after {
        content: "";
        clear: both;
        display: table;
    }
</style>
