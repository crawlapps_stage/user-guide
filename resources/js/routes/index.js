import Vue from 'vue';
import VueRouter from 'vue-router';
Vue.use(VueRouter);

const routes = [
    {
        path:'/',
        component: require('../components/pages/List').default,
        name:'list',
        meta: {
            title: 'List',
            ignoreInMenu: 0,
            displayRight: 0,
            dafaultActiveClass: '',
        },
    },
    {
        path:'/ruleset/:id?',
        component: require('../components/pages/CreateEdit').default,
        name:'ruleset',
        meta: {
            title: 'Create a User Guide',
            title_edit: 'Edit User Guide',
            title_new: 'Create a User Guide',
            ignoreInMenu: 0,
            displayRight: 0,
            dafaultActiveClass: '',
        }
    },
    {
        path:'/pre-settings',
        component: require('../components/pages/preSetting').default,
        name:'pre-settings',
        meta: {
            title: 'Settings',
            ignoreInMenu: 0,
            displayRight: 0,
            dafaultActiveClass: '',
        },
    },
    {
        path:'/settings',
        component: require('../components/pages/Settings').default,
        name:'settings',
        meta: {
            title: 'Settings',
            ignoreInMenu: 1,
            displayRight: 0,
            dafaultActiveClass: '',
        },
    },
    {
        path:'/page-settings',
        component: require('../components/pages/PageSettings').default,
        name:'page-settings',
        meta: {
            title: 'Page Settings',
            ignoreInMenu: 1,
            displayRight: 0,
            dafaultActiveClass: '',
        },
    },
    {
        path:'/choose-plan',
        // component: require('../components/pages/PlanPricing').default,
        name:'plan-pricing',
        meta: {
            title: 'Plan & Pricing',
            ignoreInMenu: 0,
            displayRight: 0,
            dafaultActiveClass: '',
        },
    },
    {
        path:'/installation-guide',
        component: require('../components/pages/InstallationGuide').default,
        name:'installation-guide',
        meta: {
            title: 'Installation Guide',
            ignoreInMenu: 0,
            displayRight: 0,
            dafaultActiveClass: '',
        },
    },
    // {
    //     path:'/help',
    //     // component: require('../components/pages/PlanPricing').default,
    //     name:'help',
    //     meta: {
    //         title: 'Help',
    //         ignoreInMenu: 0,
    //         displayRight: 0,
    //         dafaultActiveClass: '',
    //     },
    // },
];


// This callback runs before every route change, including on page load.


const router = new VueRouter({
    mode:'history',
    routes,
    scrollBehavior() {
        return {
            x: 0,
            y: 0,
        };
    },

});

export default router;
