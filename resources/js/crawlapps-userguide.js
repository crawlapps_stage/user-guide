const host = process.env.MIX_APP_URL;
const apiEndPoint = host + '/api';

var shopifyDomain = Shopify.shop;
var frontendDomain = window.location.origin;
var jqueryLoaded = 0;
if(!window.jQuery)
{
    var script = document.createElement('script');
    script.type = "text/javascript";
    script.src = "https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js";
    document.getElementsByTagName('head')[0].appendChild(script);
    jqueryLoaded = 1;
}

var shop_data = "", self = "";
Window.crawlapps_userguide = {
    init: function () {
        shop_data = JSON.parse(document.getElementById('crawlapps_userguide_shop_data').innerHTML);
        self = Window.crawlapps_userguide;
        self.initScript();
    },
    initScript() {
        if (shop_data.template == 'product') {
            self.getData();
        } else if (shop_data.template == 'page') {
            self.getPageData();
        }

        $(document).on('click', '#crawlapps_userguide_pdf', function () {
            let link = $(this).data('value');
            let guide_id = $(this).data('guide-id');
            let guide_type = $(this).data('guide-type');
            if (guide_type === 0) {
                window.open(link, "_self");
            }

            let aPIEndPoint = `${apiEndPoint}/${shopifyDomain}/page-pdfdownload?id=` + guide_id;
            $.ajax({
                method: "get",
                url: aPIEndPoint,
                contentType: 'application/json;',
                success:function (response,success,header) {
                },
            });
        });
    },
    getData() {
        // let type = $(".crawlapps_userguide_code:first").data('type');
        let len = $('.crawlapps_userguide_code').length;
        let type = [];
        if (len > 0) {
            $('.crawlapps_userguide_code').each(function (index, value) {
                type.push($(this).data('type'));
            });
        } else {
            type = 'undefined';
        }

        let aPIEndPoint = `${apiEndPoint}/${shopifyDomain}/code?product_id=` + shop_data.product.id + "&type=" + type;
        $.ajax({
            method: "get",
            url: aPIEndPoint,
            contentType: 'application/json;',
            success: function (response, success, header) {
                if (response) {
                    if (type !== 'undefined')
                        $('.crawlapps_userguide_code').each(function (index, value) {
                            let t = $(this).data('type');
                            if (t === 'button') {
                                $(this).html(response['button']);
                            } else if (t === 'link') {
                                $(this).html(response['link']);
                            }
                        });
                    if (response['floating']) {
                        $("body").prepend(response['floating']);
                    } else
                        $("body").prepend(response['floating']);
                }
            },
        });
    },
    getPageData(search = '') {
        let base = self;
        let url = window.location.href;
        if (url.includes("/")) {
            let basename = url.split('/').pop();
            basename = basename.replace('#', '');
            let aPIEndPoint = `${apiEndPoint}/${shopifyDomain}/page?p=${basename}&s=${search}`;
            $.ajax({
                method: "get",
                url: aPIEndPoint,
                contentType: 'application/json;',
                success: function (response, success, header) {
                    if (response !== '') {
                        document.getElementsByTagName('main')[0].innerHTML = response;
                        // document.getElementsByClassName('main-content')[0].innerHTML = response;
                    }
                    base.addScripts();
                    base.addSearch();
                },
            });
        }
    },
    addScripts() {
        $('#user_guide_script').remove();
        let target = document.getElementsByTagName('head')[0];
        let script_data = `var acc = document.getElementsByClassName("accordion");
                            var accPanel = document.querySelectorAll(".panel");
                            var i;
                                for (i = 0; i < acc.length; i++) {
                                acc[i].addEventListener("click", function() {
                                    this.classList.toggle("active");
                                    var panel = this.nextElementSibling;
                                    if (panel.style.maxHeight) {
                                        hidePanels();
                                    } else {
                                       showPanel(this);
                                    }
                                });
                            }
                            // Function to Show a Panel
                            function showPanel(elem) {
                              hidePanels();
                              var panel = elem.nextElementSibling;
                              elem.classList.add("active");
                              panel.style.maxHeight =   panel.scrollHeight + "px";
                            }
                            function hidePanels() {
                              for (let i = 0; i < accPanel.length; i++) {
                                  accPanel[i].style.maxHeight = null;
                                  acc[i].classList.remove("active");
                              }
                            }`;
        var newScript = document.createElement("script");
        var inlineScript = document.createTextNode(script_data);
        newScript.id = 'user_guide_script';
        newScript.appendChild(inlineScript);
        target.appendChild(newScript);
    },
    addSearch() {
        let base = this;
        document.getElementById("btn-search-guide").addEventListener("click", function () {
            let value = document.getElementById('guide-search-id').value;
            base.getPageData(value);
        });
    },
};

if(jqueryLoaded){
    setTimeout(function(){
        $(document).ready(function () {
            Window.crawlapps_userguide.init();
        });
    },2000);
}else{
    $(document).ready(function () {
        Window.crawlapps_userguide.init();
    });
}

