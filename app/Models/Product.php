<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public function belongs_to_ruleset(){
        return $this->belongsTo(Ruleset::class,'ruleset_id','id');
    }
}
