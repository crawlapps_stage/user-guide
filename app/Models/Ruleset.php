<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ruleset extends Model
{
    public function selected_products(){
        return $this->hasMany(Product::class,'ruleset_id','id')->select('id as item_id','ruleset_id','product_title as title', 'product_image as image', 'product_id as id' );
    }
}
