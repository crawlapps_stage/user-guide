<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\Ruleset;
use App\Models\Setting;
use App\User;
use Response;
use Illuminate\Http\Request;

class PageController extends Controller
{
   public function index(Request $request, $shopifyShop){
       try{
           $shop = User::where('name', $shopifyShop)->first();
           if( $shop && $shop->page_handle == $request->p){
               if( $request->s != '' ){
                   $products = Product::with('belongs_to_ruleset')->where('user_id', $shop->id)->where('product_title', 'LIKE', "%$request->s%")->get();
               }else{
                   $products = Product::with('belongs_to_ruleset')->where('user_id', $shop->id)->get();
               }

                $page_settings = Setting::where('user_id', $shop->id)->where('type', 'page')->first();
                $style = json_decode($page_settings->style, true);
                $search_value = $request->s;
               return Response()->view('api.guides', compact('products','style', 'search_value'), 200)->withHeaders([
                   'Content-Type'=>'text/html',
               ]);
            }
       }catch( \Exception $e ){
           return response::json(['data' => $e->getMessage()], 422);
       }
   }

   public function downloadPDF(Request $request){
       try{
           $ruleset = Ruleset::find($request->id);
           if( $ruleset ){
               $ruleset->downloads+=1;
               $ruleset->save();
           }
           return response::json(['data' => 'success'], 200);
       }catch(\Exception $e){
           return response::json(['data' => $e->getMessage()], 422);
       }
   }
}
