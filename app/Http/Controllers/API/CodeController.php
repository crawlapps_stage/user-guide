<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\Setting;
use App\User;
use Illuminate\Http\Request;
use function GuzzleHttp\Promise\settle;

class CodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $shopifyShop)
    {
        $product_id = $request->product_id;
//        $type = ($request->type != "undefined") ? $request->type:'floating';
        $type = ($request->type != "undefined") ? explode(',', $request->type) : 'floating';
        $shop = User::where('name', $shopifyShop)->first();
        $data = false;
        if($shop && @$shop->status){
            $product = Product::with('belongs_to_ruleset')->where('product_id',$product_id)->where('user_id', $shop->id)->first();
            if($product) {
                $ruleset = $product->belongs_to_ruleset;
                if($ruleset->status){
                    $style = Setting::where('user_id', $shop->id)->where('type', 'user')->first();
                    $style = json_decode($style->style,true);

                    if( $ruleset->guide_type == 0 ){
                        $link = 'href="#" data-value=' . config('app.url')."/api/pdf/".$shop->id."/".$product->product_id . ' data-guide-type='. $ruleset->guide_type .' data-guide-id=' . $ruleset->id . ' ';
                    }else{
//                        $link = \Storage::get('public', '/uploads/'.$ruleset->description);
                        $link = 'href="' . \Storage::disk('public')->url('uploads/'.$ruleset->description) . '" download target="_blank"  data-guide-type='. $ruleset->guide_type .' data-guide-id=' . $ruleset->id . ' ';
                    }

                    if( is_array( $type ) ){
                        if( $style['float']['status'] == 1 ){
                            array_push($type, 'floating');
                        }
                        foreach ( $type as $key=>$val ){
                            $data[$val] = $this->{$val}($style,$link);
                        }
                    }else{
                        $data[$type] = $this->{$type}($style,$link);
                    }
                }
            }
        }
        return response($data);
    }
    public function button($style,$link) {
        $button = $style['button'];
        $shadow = ( $button['shadow_type'] == 'inset' ) ? 'inset' : '';
        $css = "background-color:{$button['bg_color']};color:{$button['text_color']};font-size:{$button['text_size']}px;border-width:{$button['border_size']}px;border-radius:{$button['border_radius']}px;padding:{$button['padding_top']}px {$button['padding_right']}px {$button['padding_bottom']}px {$button['padding_left']}px;border-color:{$button['border_color']};box-shadow:{$shadow} {$button['shadow_x']}px {$button['shadow_y']}px {$button['shadow_blur']}px {$button['shadow_spread']}px {$button['shadow_color']}";

        $mouseover = "this.style.borderColor='{$button['border_color_hover']}'";
        $mouseout = "this.style.borderColor='{$button['border_color']}'";

//        $html = '<button type="button" id="crawlapps_userguide_pdf" data-value="'.$link.'" onMouseOver="'.$mouseover.'" onMouseOut="'.$mouseout.'" style="'.$css.'">'.$button["label"].'</button>';
        $html = '<a type="button" id="crawlapps_userguide_pdf"'. $link .'onMouseOver="'.$mouseover.'" onMouseOut="'.$mouseout.'" style="'.$css.'">'.$button["label"].'</a>';
        return $html;
    }
    public function link($style,$link){
        $style = $style['link'];
        $css = "color:{$style['text_color']};text-decoration:{$style['text_decoration_line']} {$style['text_decoration_style']} {$style['text_decoration_color']};font-size:{$style['text_size']}px;font-weight:{$style['text_weight']}";
        $html = '<a id="crawlapps_userguide_pdf"'. $link .'style="'.$css.'">'.$style["label"].'</a>';
        return $html;
    }
    public function floating($style, $link){
        $float = $style['float'];
        $shadow = ( $float['shadow_type'] == 'inset' ) ? 'inset' : '';
        $css = "bottom: {$float['bottom_margin']}px;z-index: 9;position: fixed;right: {$float['right_margin']}px;";
        $css .= $float['align'] == 1 ? "" : "left:0;" ;
        $css .= "background-color:{$float['bg_color']};color:{$float['text_color']};font-size:{$float['text_size']}px;border-width:{$float['border_size']}px;border-radius:{$float['border_radius']}px;padding:{$float['padding_top']}px {$float['padding_right']}px {$float['padding_bottom']}px {$float['padding_left']}px;border-color:{$float['border_color']};box-shadow:{$shadow} {$float['shadow_x']}px {$float['shadow_y']}px {$float['shadow_blur']}px {$float['shadow_spread']}px {$float['shadow_color']}";

        $mouseover = "this.style.borderColor='{$float['border_color_hover']}'";
        $mouseout = "this.style.borderColor='{$float['border_color']}'";
        $html = '<a type="button" class="crawlapps_userguide_pdf float_pdf" id="crawlapps_userguide_pdf"'. $link .'  onMouseOver="'.$mouseover.'" onMouseOut="'.$mouseout.'" style="'.$css.'">'.$float["label"].'</a>';
        return $html;
    }

    public function pdf($user_id, $product_id){
        $shop = User::find($user_id);
        if($shop && @$shop->status) {
            $product = Product::with('belongs_to_ruleset')->where('product_id', $product_id)->where('user_id', $shop->id)->first();
            if ($product) {
                $ruleset = $product->belongs_to_ruleset;
                if ($ruleset->status) {
                    $html = $ruleset->description;
                    $file_name = \Str::slug($ruleset->title);
                    $pdf = \PDF::loadView('welcome', [ "html" => $html]);
                    return $pdf->setPaper('a4')->setOption('margin-bottom', 0)->download($file_name.".pdf");
                    //return view('welcome', compact('html'));
                }
            }
        }
    }

    public function storeImages(Request $request){
        dd($request);
        // never delete
    }
}
