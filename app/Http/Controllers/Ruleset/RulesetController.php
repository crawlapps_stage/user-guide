<?php

namespace App\Http\Controllers\Ruleset;

use App\Http\Controllers\Controller;
use App\Http\Requests\RulesetRequest;
use App\Models\Product;
use App\Models\Ruleset;
use App\Models\Setting;
use App\Traits\ImageTrait;
use Illuminate\Http\Request;

class RulesetController extends Controller
{
    use ImageTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->has('api')) {
            return $this->apiIndex($request);
        }
    }

    public function apiIndex(Request $request)
    {
        $shop = \Auth::user();
        $entities = [];

        if ($request->s != '') {
            $entities = Ruleset::where('user_id', $shop->id)->where('title', 'LIKE',
                "%$request->s%")->orderBy('created_at', 'desc')->get();
        } else {
            $entities = Ruleset::where('user_id', $shop->id)->orderBy('created_at', 'desc')->get();
        }
        $entity = [];
        if ($entities) {
            $entity = $entities->map(function ($name) {
                return [
                    'id' => $name->id,
                    'status' => $name->status,
                    'title' => $name->title,
                    'description' => $name->description,
                    'guide_type' => $name->guide_type,
                    'downloads' => $name->downloads,
                    'created_at' => date("Y-m-d H:i:s", strtotime($name->created_at)),
                ];
            })->toArray();
        }
        return response(['entities' => $entity, "app_status" => $shop->status, "plan_id" => $shop->plan_id], 200);
    }

    public function store(RulesetRequest $request)
    {
        $data = json_decode($request->data, true);
        $shop = \Auth::user();

        $shop_name = str_replace('.myshopify.com', '', $shop->name);
        if ($data['guide_type'] == 0) {
            $des = $data['description'];
        } else {
            $des = $shop_name.'/'.ImageTrait::makeImage($request->file, "/public/uploads/$shop_name");
        }

        $entity = new Ruleset();
        $entity->title = $data['ruleset_name'];
        $entity->status = $data['status'];
        $entity->description = $des;
        $entity->user_id = $shop->id;
        $entity->guide_type = $data['guide_type'];
        $entity->save();
        $this->addProducts($data, $entity);
        return response(['message' => 'Successfully Created'], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $entity = Ruleset::with('selected_products')->find($id);
        return response($entity, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(RulesetRequest $request, $id)
    {
        $data = json_decode($request->data, true);
        $shop = \Auth::user();

        $shop_name = str_replace('.myshopify.com', '', $shop->name);
        if ($data['guide_type'] == 0) {
            $des = $data['description'];
        } else {
            if (gettype($request->file) === 'object') {
                $des = $shop_name.'/'.ImageTrait::makeImage($request->file, "/public/uploads/$shop_name");
            } else {
                $des = $data['description'];
            }
        }

        $entity = Ruleset::find($id);
        $entity->title = $data['ruleset_name'];
        $entity->status = $data['status'];
        $entity->description = $des;
        $entity->user_id = $shop->id;
        $entity->guide_type = $data['guide_type'];
        $entity->save();
        $this->addProducts($data, $entity);
        return response(['message' => 'Successfully Updated'], 200);
    }

    public function addProducts($data, $entity, $id = null)
    {
        Product::where('ruleset_id', $entity->id)->delete();
        $data['selectedProducts'] = array_filter($data['selectedProducts']);
        foreach ($data['selectedProducts'] as $key => $val) {
            $product = new Product;
            $product->user_id = $entity->user_id;
            $product->ruleset_id = $entity->id;
            $product->product_id = $val['id'];
            $product->product_title = $val['title'];
            $product->product_image = $val['image'];
            $product->save();
        }

        return true;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $shop = \Auth::user();
        $ruleset_product = ModelSyncVariant::where('user', $shop->id)->where('ruleset_id', $id);
        $ruleset_product->update([
            'ruleset_id' => null, 'total_sale' => 0, 'current_level' => 0, 'current_price' => \DB::raw('original_price')
        ]);
        $entity = RuleSet::find($id)->delete();
        return response(['data' => []], 200);
    }

    public function check(Request $request)
    {
        $shop = \Auth::user();
        $entity = Product::where('user_id', $shop->id)->get()->toArray();
        $entity = array_column($entity, 'product_id');;
        $prepareArray = [];
        $c = 0;
        $notfound_image = asset('images/not_found.png');

        foreach ($request['data'] as $key => $val) {
            $val['id'] = str_replace("gid://shopify/Product/", "", $val['id']);
            if (!in_array($val['id'], $entity)) {
                $prepareArray[$c]['id'] = $val['id'];
                $prepareArray[$c]['title'] = $val['title'];
                $prepareArray[$c++]['image'] = @($val['images'][0]['originalSrc']) ? $val['images'][0]['originalSrc'] : $notfound_image;
            }
        }

        return \Response::json([
            'data' => $prepareArray,
            'total_selected' => count($request['data']),
            'total_added' => $c,
        ], 200);
    }


    public function deleteProduct($id, Request $request)
    {
        $shop = \Auth::user();
        Product::find($id)->delete();
        return response(['message' => "Successfully Deleted."], 200);
    }

    public function appStatus(Request $request)
    {
        $shop = \Auth::user();
        $shop->status = $request->app_status;
        $shop->save();

        $status = $shop->status ? "enable" : "disable";

        $message = "App status changed to {$status}.";

        return response(['message' => $message], 200);
    }

    public function storeSetting(Request $request)
    {
        $shop = \Auth::user();
        if ($request->type == 'user') {
            $theme = $request->theme;
            if ($shop->theme_id != $theme['id']) {
                $res = $this->createSnippet($theme);
                if( $res ){
                    $shop->theme_id = $theme['id'];
                    $shop->theme_name = $theme['name'];
                    $shop->save();
                }
            }
        }
        $entity = Setting::where('user_id', $shop->id)->where('type', $request->type)->first();
        $entity = ($entity) ? $entity : new Setting;
        $entity->user_id = $shop->id;
        $entity->style = json_encode($request->settings);
        $entity->type = $request->type;

        $entity->save();
        return response(['message' => "Successfully saved."], 200);
    }

    public function getSetting(Request $request)
    {
        $shop = \Auth::user();

        if ($request->type == 'user') {
            $parameter['fields'] = 'id,name';
            $sh_themes = $shop->api()->rest('GET', 'admin/themes.json', $parameter);
            $theme = [];
            if (!$sh_themes->errors) {
                $themes = $sh_themes->body->themes;
                foreach ($themes as $key => $val) {
                    $theme[$key]['id'] = $val->id;
                    $theme[$key]['name'] = $val->name;
                    $theme[$key]['is_active'] = ($val->id == $shop->theme_id) ? 1 : 0;
                }
            }
            $curr_theme['id'] = $shop->theme_id;
            $curr_theme['name'] = $shop->theme_name;
        }
        $entity = Setting::where('user_id', $shop->id)->where('type', $request->type)->first();
        return \Response::json([
            'themes' => ($request->type == 'user') ? $themes : '',
            'curr_theme' => ($request->type == 'user') ? $curr_theme : '',
            'settings' => json_decode(@$entity->style),
            'page_url' => ($request->type == 'page') ? 'https://'.$shop->name.'/pages/'.$shop->page_handle : '',

        ], 200);
    }

    public function storePDF(Request $request)
    {
        // never delete
    }

    public function getPlanDetails()
    {
        try {
            $shop = \Auth::user();
            $total_guides = Ruleset::select('id', 'status', 'title', 'created_at','downloads')->where('user_id', $shop->id)->count();
            $plan_id = $shop->plan_id;
            $data['is_display_link'] = $total_guides >= 1;
            $data['page_url'] = ($total_guides >= 1) ? 'https://'.$shop->name.'/pages/'.$shop->page_handle : '';
            if ($plan_id == 1 && $total_guides >= 15) {
                $data['msg'][0] = "You can't create more user guide, please upgrade your plan with pro plan.";
                $data['is_disable'] = 1;
            } else {
                $data['msg'] = '';
                $data['is_disable'] = 0;
            }
            return response(['message' => $data], 200);
        } catch (\Exception $e) {
            return response(['message' => $e], 422);
        }
    }

    public function createSnippet($theme)
    {
        try {
            $shop = \Auth::user();
            $value = <<<EOF
        <script id="crawlapps_userguide_shop_data" type="application/json">
            {
                "shop": {
                    "domain": "{{ shop.domain }}",
                    "permanent_domain": "{{ shop.permanent_domain }}",
                    "url": "{{ shop.url }}",
                    "secure_url": "{{ shop.secure_url }}",
                    "money_format": {{ shop.money_format | json }},
                    "currency": {{ shop.currency | json }}
                },
                "customer": {
                    "id": {{ customer.id | json }},
                    "tags": {{ customer.tags | json }}
                },
                "cart": {{ cart | json }},
                "template": "{{ template | split: "." | first }}",
                "product": {{ product | json }},
                "collection": {{ collection.products | json }}
            }
        </script>
EOF;
            $parameter['asset']['key'] = 'snippets/crawlapps-userguide.liquid';
            $parameter['asset']['value'] = $value;
            $asset = $shop->api()->rest('PUT', 'admin/themes/'.$theme['id'].'/assets.json', $parameter);

            if( !$asset->errors ){
                $asset = $shop->api()->rest('GET', 'admin/themes/'.$theme['id'].'/assets.json',
                    ["asset[key]" => 'layout/theme.liquid']);
                if (@$asset->body->asset) {
                    $asset = $asset->body->asset->value;
                    if (!strpos($asset, "{% include 'crawlapps-userguide' %}</head>")) {
                        $asset = str_replace('</head>', "{% include 'crawlapps-userguide' %}</head> ", $asset);
                    }

                    $parameter['asset']['key'] = 'layout/theme.liquid';
                    $parameter['asset']['value'] = $asset;
                    $asset = $shop->api()->rest('PUT', 'admin/themes/'.$theme['id'].'/assets.json', $parameter);
                    if( !$asset->errors ){
                        return true;
                    }else{
                        return false;
                    }
                }
                return true;
            }else{
                return false;
            }

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
}
