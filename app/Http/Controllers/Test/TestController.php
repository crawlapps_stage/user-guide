<?php

namespace App\Http\Controllers\Test;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TestController extends Controller
{
    private $shop = '';
    private $i = 0;
    public function index(){
//        try{
//           $this->shop = \Auth::user();
//           $this->getPage('user-guide');
//        }catch ( \Exception $e ){
//            dd($e);
//        }
    }

    public function getPage($handle){
        try{
            $parameter['handle'] = $handle;
            $result = $this->shop->api()->rest('GET', '/admin/api/2020-04/pages.json',$parameter);
            if( !$result->errors ){
                $page = $result->body->pages;
            }
            if( empty( $page ) ){
                dump('create page');
                $page = [
                    "page" => [
                        "title" => 'User Guide',
                        "body_html" => "User guide Page"
                    ]
                ];
                $result = $this->shop->api()->rest('POST', '/admin/api/2020-04/pages.json',$page);

                if( !$result->errors ){
                    $this->shop->page_id = $result->body->page->id;
                    $this->shop->page_handle = $result->body->page->handle;
                    $this->shop->save();
                }else{
                    \Log::info($result);
                }

            }else{
                dump($handle . ' page available');
                $this->i++;
                $handle = 'user-guide-' . $this->i;
                $this->getPage($handle);
            }
        }catch( \Exception $e ){
            dd($e);
        }
    }
}
