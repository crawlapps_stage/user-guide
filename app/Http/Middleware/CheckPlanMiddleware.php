<?php

namespace App\Http\Middleware;

use Closure;

class CheckPlanMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
//        $shop = \Auth::user();

//        if(is_null($shop->plan_id) && !$shop->isGrandfathered() && !$shop->isFreemium()){
//            return redirect("/choose-plan/0");
//        }elseif ( $shop->plan_id == 1 ){
//            $to = date('Y-m-d H:s:i');
//            $from = $shop->created_at;
//            $diff = strtotime($to) - strtotime($from);
//
//            // 1 day = 24 hours
//            // 24 * 60 * 60 = 86400 seconds
//            $days = abs(round($diff / 86400));
//            if( $days > 30 ){
//                return redirect("/choose-plan/1");
//            }else{
//                return $next($request);
//            }
//        }
        return $next($request);
    }
}
