<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;
use Route;

class RulesetRequest extends FormRequest
{
    public static $rules = [];

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function validator($factory)
    {
        return $factory->make(
            $this->sanitize(), $this->container->call([$this, 'rules']), $this->messages()
        );
    }

    public function sanitize()
    {
        if( gettype($this->data) == 'string' ){
            $data  = (array)json_decode($this->data, true);
            if( !empty($data) ){
                $this->merge([
                    'selectedProducts' => $data['selectedProducts'],
                    'ruleset_name' => $data['ruleset_name'],
                    'description' => $data['description'],
                ]);
                return $this->all();
            }else{
                return [];
            }
        }else{
            return [];
        }
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = self::$rules;
//        $rules = (array)json_decode($this->file);
        switch (Route::currentRouteName()) {
            case 'ruleset.store':
            {
                $inputes = $this->all();
                $rules['selectedProducts'] = "required|array|min:1";
                $rules['ruleset_name'] = "required";
                $rules['description'] = "required";
                return $rules;
            }
            case 'ruleset.update':
            {
                return $rules;
            }
            default:
                break;
        }
        return [];
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'selectedProducts.required'  => 'Select atleast 1 product.',
        ];
    }

    /**
     * Handle a failed validation attempt.
     *
     * @param  \Illuminate\Contracts\Validation\Validator $validator
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function failedValidation(Validator $validator)
    {
        if ($this->ajax() || $this->wantsJson()) {
            $response = new JsonResponse($validator->errors(), 422);
            throw new ValidationException($validator, $response);
        }

        throw (new ValidationException($validator))
            ->errorBag($this->errorBag)
            ->redirectTo($this->getRedirectUrl());
    }
}
