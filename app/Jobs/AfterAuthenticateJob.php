<?php

namespace App\Jobs;

use App\Models\Setting;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class AfterAuthenticateJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $i = 0;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $shop = \Auth::user();
        $userstyle = '{"link": {"label": "User Guide link", "text_size": 26, "text_color": "#000000", "text_weight": "700", "text_decoration": "underline solid black", "text_decoration_line": "none", "text_decoration_color": "#000000", "text_decoration_style": "solid"}, "float": {"align": 1, "label": "User Guide", "status": 1, "bg_color": "#ffffff", "shadow_x": 8, "shadow_y": 4, "text_size": "15", "text_color": "#000000", "border_size": "1", "padding_top": "5", "shadow_blur": 11, "shadow_type": "outset", "border_color": "#000000", "padding_left": "5", "right_margin": 20, "shadow_color": "#b2a9a9", "border_radius": 6, "bottom_margin": 20, "padding_right": "5", "shadow_spread": 6, "padding_bottom": "5", "border_color_hover": "#000000"}, "button": {"label": "User Guide Button", "bg_color": "#ffffff", "shadow_x": 0, "shadow_y": 0, "text_size": "15", "text_color": "#000000", "border_size": "1", "padding_top": "5", "shadow_blur": 13, "shadow_type": "outset", "border_color": "#b2a9a9", "padding_left": "5", "shadow_color": "#b2a9a9", "border_radius": "5", "padding_right": "5", "shadow_spread": 4, "padding_bottom": "5", "border_color_hover": "#AFA7A7"}}';

        $pagestyle = '{"link": {"label": "Download the PDF", "status": 1, "text_size": "15", "text_color": "#000000", "font_family": "inherit", "text_weight": "inherit", "text_decoration_line": "none", "text_decoration_color": "#000000", "text_decoration_style": "solid"}, "button": {"label": "User Guide", "status": 0, "bg_color": "#ffffff", "text_size": "15", "text_color": "#000000", "border_size": "1", "padding_top": "5", "border_color": "#000000", "padding_left": "5", "border_radius": "6", "padding_right": "5", "padding_bottom": "5", "border_color_hover": "#000000"}, "search": {"label": "Search", "status": 1, "bg_color": "#f1f1f1", "text_size": "15", "text_color": "#000000", "border_size": "1", "padding_top": "10", "sb_bg_color": "#3d4246", "border_color": "#A9A2A2", "padding_left": "10", "sb_text_size": 13, "border_radius": "6", "padding_right": "10", "sb_text_color": "#FFFFFF", "padding_bottom": "10", "sb_border_size": "1", "sb_padding_top": "10", "sb_border_color": "#3d4246", "sb_padding_left": "10", "sb_border_radius": "6", "sb_padding_right": "10", "sb_padding_bottom": "10"}, "product": {"text_size": 15, "text_color": "#000000", "font_family": "inherit", "text_weight": "bold", "text_decoration_line": "none", "text_decoration_color": "#000000", "text_decoration_style": "solid", "bg_color": "#D8D8D8", "bg_color_on_hover": "#9E9A9A"}, "heading1": {"label": "User Guides", "text_size": 33, "text_color": "#000000", "text_weight": "900", "text_decoration_line": "underline", "text_decoration_color": "#000000", "text_decoration_style": "solid"}, "heading2": {"label": "Which product did you bought?", "text_size": 23, "text_color": "#000000", "text_weight": "400", "text_decoration_line": "none", "text_decoration_color": "#000000", "text_decoration_style": "solid"}}';

        $type = ['user' => $userstyle, 'page' => $pagestyle];

        foreach( $type as $key=>$val ){
            $entity = Setting::where('user_id',$shop->id)->where('type', $key)->first();
            if(empty($entity)){
                $entity = Setting::where('user_id',$shop->id)->where('type', $key)->firstOrNew();
                $entity->user_id = $shop->id;
                $entity->style = $val;
                $entity->type = $key;
                $entity->save();
            }
        }
        $this->snippet();

        \Log::info(json_encode($shop));

        if( $shop->page_id == '' || $shop->page_id == null ){
            $this->getPage('user-guide');
        }
    }
    public function snippet(){

        $shop = \Auth::user();
        $sh_theme = $shop->api()->rest('GET', 'admin/themes.json',['role' => 'main']);
        $main_theme = $sh_theme->body->themes[0]->id;

        $shop->theme_id = $main_theme;
        $shop->theme_name = $sh_theme->body->themes[0]->name;
        $shop->save();

            $value = <<<EOF
        <script id="crawlapps_userguide_shop_data" type="application/json">
            {
                "shop": {
                    "domain": "{{ shop.domain }}",
                    "permanent_domain": "{{ shop.permanent_domain }}",
                    "url": "{{ shop.url }}",
                    "secure_url": "{{ shop.secure_url }}",
                    "money_format": {{ shop.money_format | json }},
                    "currency": {{ shop.currency | json }}
                },
                "customer": {
                    "id": {{ customer.id | json }},
                    "tags": {{ customer.tags | json }}
                },
                "cart": {{ cart | json }},
                "template": "{{ template | split: "." | first }}",
                "product": {{ product | json }},
                "collection": {{ collection.products | json }}
            }
        </script>
EOF;
        $parameter['asset']['key'] = 'snippets/crawlapps-userguide.liquid';
        $parameter['asset']['value'] = $value;
        $asset = $shop->api()->rest('PUT', 'admin/themes/'.$main_theme.'/assets.json',$parameter);


        $asset = $shop->api()->rest('GET', 'admin/themes/'.$main_theme.'/assets.json',["asset[key]" => 'layout/theme.liquid']);
        if(@$asset->body->asset) {
            $asset = $asset->body->asset->value;
            if(!strpos($asset ,"{% include 'crawlapps-userguide' %}</head>")) {
                $asset = str_replace('</head>',"{% include 'crawlapps-userguide' %}</head> ",$asset);
            }

            $parameter['asset']['key'] = 'layout/theme.liquid';
            $parameter['asset']['value'] = $asset;
            $asset = $shop->api()->rest('PUT', 'admin/themes/'.$main_theme.'/assets.json',$parameter);
        }
    }

    public function getPage($handle){
        \Log::info('========================= GET PAGE ===========================');
        try{
            $shop = \Auth::user();
            $parameter['handle'] = $handle;
            $result = $shop->api()->rest('GET', '/admin/api/2020-04/pages.json',$parameter);
            if( !$result->errors ){
                $page = $result->body->pages;
            }
            if( empty( $page ) ){
                \Log::info('create page ' . $handle);
                $page = [
                    "page" => [
                        "title" => 'User Guide',
                        "body_html" => "User guide Page"
                    ]
                ];
                $result = $shop->api()->rest('POST', '/admin/api/2020-04/pages.json',$page);
                if( !$result->errors ){
                    $shop->page_id = $result->body->page->id;
                    $shop->page_handle = $result->body->page->handle;
                    $shop->save();
                }else{
                    \Log::info(json_encode($result));
                }
            }else{
                \Log::info($handle . ' page available');
                $this->i++;
                $handle = 'user-guide-' . $this->i;
                $this->getPage($handle);
            }
        }catch( \Exception $e ){
            \Log::info('========================= ERROR :: GET PAGE ===========================');
            \Log::info($e);
        }
    }
}
