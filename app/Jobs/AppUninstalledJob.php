<?php

namespace App\Jobs;
use App\User;
use Osiset\ShopifyApp\Actions\CancelCurrentPlan;
use Osiset\ShopifyApp\Contracts\Commands\Shop as IShopCommand;
use Osiset\ShopifyApp\Contracts\Queries\Shop as IShopQuery;

class AppUninstalledJob extends \Osiset\ShopifyApp\Messaging\Jobs\AppUninstalledJob
{
    /**
     * Execute the job.
     *
     * @return bool
     */
    public function handle(
        IShopCommand $shopCommand,
        IShopQuery $shopQuery,
        CancelCurrentPlan $cancelCurrentPlanAction
    ): bool {
        \Log::info('------------------- App uninstalled JOB-------------------');
        // Get the shop
        /*$shop = $shopQuery->getByDomain($this->domain);
        \Log::info(json_encode($this->data->myshopify_domain));
        \Log::info(json_encode($shop));
        $shopId = $shop->getId();

        // Cancel the current plan
        $cancelCurrentPlanAction($shopId);

        // Purge shop of token, plan, etc.
        $shopCommand->clean($shopId);*/

        // Soft delete the shop.
        //$shopCommand->softDelete($shopId);
        User::where('name', $this->data->myshopify_domain)->forceDelete();
        return true;
    }
}
