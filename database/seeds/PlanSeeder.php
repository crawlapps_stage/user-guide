<?php

use Illuminate\Database\Seeder;

class PlanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS=0');
        \DB::table('plans')->truncate();
        \DB::statement('SET FOREIGN_KEY_CHECKS=1');

        \DB::table('plans')->insert(
            [
                [
                    'type' => "RECURRING",
                    'name' => 'Basic Plan',
                    'price' => 5.00,
                    'capped_amount' => 0.00,
                    'terms' => 'Basic Plan',
                    'trial_days' => 15,
                    'test' => 0,
                    'on_install' => 1
                ],
                [
                    'type' => "RECURRING",
                    'name' => 'Pro Plan',
                    'price' => 10.00,
                    'capped_amount' => 0.00,
                    'terms' => 'Pro Plan',
                    'trial_days' => 15,
                    'test' => 0,
                    'on_install' => 1
                ],
            ]
        );
    }
}
