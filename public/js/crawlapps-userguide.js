/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 2);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/js/crawlapps-userguide.js":
/*!*********************************************!*\
  !*** ./resources/js/crawlapps-userguide.js ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

var host = "https://user-guide.test";
var apiEndPoint = host + '/api';
var shopifyDomain = Shopify.shop;
var frontendDomain = window.location.origin;
var jqueryLoaded = 0;

if (!window.jQuery) {
  var script = document.createElement('script');
  script.type = "text/javascript";
  script.src = "https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js";
  document.getElementsByTagName('head')[0].appendChild(script);
  jqueryLoaded = 1;
}

var shop_data = "",
    self = "";
Window.crawlapps_userguide = {
  init: function init() {
    shop_data = JSON.parse(document.getElementById('crawlapps_userguide_shop_data').innerHTML);
    self = Window.crawlapps_userguide;
    self.initScript();
  },
  initScript: function initScript() {
    if (shop_data.template == 'product') {
      self.getData();
    } else if (shop_data.template == 'page') {
      self.getPageData();
    }

    $(document).on('click', '#crawlapps_userguide_pdf', function () {
      var link = $(this).data('value');
      var guide_id = $(this).data('guide-id');
      var guide_type = $(this).data('guide-type');

      if (guide_type === 0) {
        window.open(link, "_self");
      }

      var aPIEndPoint = "".concat(apiEndPoint, "/").concat(shopifyDomain, "/page-pdfdownload?id=") + guide_id;
      $.ajax({
        method: "get",
        url: aPIEndPoint,
        contentType: 'application/json;',
        success: function success(response, _success, header) {}
      });
    });
  },
  getData: function getData() {
    // let type = $(".crawlapps_userguide_code:first").data('type');
    var len = $('.crawlapps_userguide_code').length;
    var type = [];

    if (len > 0) {
      $('.crawlapps_userguide_code').each(function (index, value) {
        type.push($(this).data('type'));
      });
    } else {
      type = 'undefined';
    }

    var aPIEndPoint = "".concat(apiEndPoint, "/").concat(shopifyDomain, "/code?product_id=") + shop_data.product.id + "&type=" + type;
    $.ajax({
      method: "get",
      url: aPIEndPoint,
      contentType: 'application/json;',
      success: function success(response, _success2, header) {
        if (response) {
          if (type !== 'undefined') $('.crawlapps_userguide_code').each(function (index, value) {
            var t = $(this).data('type');

            if (t === 'button') {
              $(this).html(response['button']);
            } else if (t === 'link') {
              $(this).html(response['link']);
            }
          });

          if (response['floating']) {
            $("body").prepend(response['floating']);
          } else $("body").prepend(response['floating']);
        }
      }
    });
  },
  getPageData: function getPageData() {
    var search = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
    var base = self;
    var url = window.location.href;

    if (url.includes("/")) {
      var basename = url.split('/').pop();
      basename = basename.replace('#', '');
      var aPIEndPoint = "".concat(apiEndPoint, "/").concat(shopifyDomain, "/page?p=").concat(basename, "&s=").concat(search);
      $.ajax({
        method: "get",
        url: aPIEndPoint,
        contentType: 'application/json;',
        success: function success(response, _success3, header) {
          if (response !== '') {
            document.getElementsByTagName('main')[0].innerHTML = response; // document.getElementsByClassName('main-content')[0].innerHTML = response;
          }

          base.addScripts();
          base.addSearch();
        }
      });
    }
  },
  addScripts: function addScripts() {
    $('#user_guide_script').remove();
    var target = document.getElementsByTagName('head')[0];
    var script_data = "var acc = document.getElementsByClassName(\"accordion\");\n                            var accPanel = document.querySelectorAll(\".panel\");\n                            var i;\n                                for (i = 0; i < acc.length; i++) {\n                                acc[i].addEventListener(\"click\", function() {\n                                    this.classList.toggle(\"active\");\n                                    var panel = this.nextElementSibling;\n                                    if (panel.style.maxHeight) {\n                                        hidePanels();\n                                    } else {\n                                       showPanel(this);\n                                    }\n                                });\n                            }\n                            // Function to Show a Panel\n                            function showPanel(elem) {\n                              hidePanels();\n                              var panel = elem.nextElementSibling;\n                              elem.classList.add(\"active\");\n                              panel.style.maxHeight =   panel.scrollHeight + \"px\";\n                            }\n                            function hidePanels() {\n                              for (let i = 0; i < accPanel.length; i++) {\n                                  accPanel[i].style.maxHeight = null;\n                                  acc[i].classList.remove(\"active\");\n                              }\n                            }";
    var newScript = document.createElement("script");
    var inlineScript = document.createTextNode(script_data);
    newScript.id = 'user_guide_script';
    newScript.appendChild(inlineScript);
    target.appendChild(newScript);
  },
  addSearch: function addSearch() {
    var base = this;
    document.getElementById("btn-search-guide").addEventListener("click", function () {
      var value = document.getElementById('guide-search-id').value;
      base.getPageData(value);
    });
  }
};

if (jqueryLoaded) {
  setTimeout(function () {
    $(document).ready(function () {
      Window.crawlapps_userguide.init();
    });
  }, 2000);
} else {
  $(document).ready(function () {
    Window.crawlapps_userguide.init();
  });
}

/***/ }),

/***/ 2:
/*!***************************************************!*\
  !*** multi ./resources/js/crawlapps-userguide.js ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /var/www/html/shopify/user-guide/resources/js/crawlapps-userguide.js */"./resources/js/crawlapps-userguide.js");


/***/ })

/******/ });